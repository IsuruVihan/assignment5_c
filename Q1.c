/*
    Isuru Harischandra
    Student ID :- 202032
    
    * This program will estimate the highest profit and the particular ticket price
*/

# include <stdio.h>

// calculate cost
int _cost(int attendance) {
    return attendance * 3 + 500;
}

// calculate revenue
int _revenue(int attendance, int ticketPrice) {
    return attendance * ticketPrice;
}

// estimated attendance
int _attendance(int ticketPrice) {
    return 180 - 4 * (ticketPrice);
}

// calculate profit
int profit(int ticketPrice) {
    int attendance = _attendance(ticketPrice);
    int revenue = _revenue(attendance, ticketPrice);
    int cost = _cost(attendance);
    return revenue - cost;
}

// start
int main() {
    printf("This program will predict the ticket price at the highest profit\n----------------------------------------------------------------");
    int ticketPrice, max = 0, maxTicket = 0;
    for (ticketPrice = 10; ticketPrice <= 40; ticketPrice += 5) {
        printf("\nTicket price = Rs.%d\nNet profit = Rs.%d\n", ticketPrice, profit(ticketPrice));
        // max profit & particular ticket price
        if (max < profit(ticketPrice)) {
            max = profit(ticketPrice);
            maxTicket = ticketPrice;
        }
    }
    printf("-------------------------\nHighest profit = Rs.%d\nTicket price = Rs.%d\n-------------------------\n", max, maxTicket);
    return 0;
}